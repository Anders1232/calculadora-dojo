#ifndef CAMADA_APRESENTACAO_HPP
#define CAMADA_APRESENTACAO_HPP

#include <gtkmm.h>

namespace CalculadoraDojo
{
	class CamadaApresentacao
	{
		public:
			CamadaApresentacao(Glib::RefPtr<Gtk::Application> app) noexcept;
			~CamadaApresentacao ();
			Glib::RefPtr<Gtk::Window> JanelaPrincipal(void);
		private:
			void AtualizarResultados(void);
			void Fechar(void);
			std::vector< Glib::ustring> ObterAutores(void) const;

			Glib::RefPtr<Gtk::Application> app;

			Glib::RefPtr<Gtk::Window> window;

			Glib::RefPtr<Gtk::SpinButton> sbOperador1;
			Glib::RefPtr<Gtk::SpinButton> sbOperador2;

			Glib::RefPtr<Gtk::ComboBoxText> cbtOperacao;

			Glib::RefPtr<Gtk::Entry> eResultado;

			Glib::RefPtr<Gtk::Label> lbOperador1;
			Glib::RefPtr<Gtk::Label> lbOperador2;
			Glib::RefPtr<Gtk::Label> lbOperacao;
			Glib::RefPtr<Gtk::Label> lbResultado;
	};
}

#endif // PRESENTATIONLAYER_HPP
