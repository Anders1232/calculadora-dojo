#include "CamadaApresentacao.hpp"
#include <gtkmm/application.h>

int main (int argc, char **argv)
{
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create (argc, argv, "org.anders1232.CalculadoraDojo");
	
	CalculadoraDojo::CamadaApresentacao calculadoraDojo(app);

	return app->run( *(calculadoraDojo.JanelaPrincipal().get() ) );
}
