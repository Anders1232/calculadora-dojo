all:
	glib-compile-resources CalculadoraDojo.gresource.xml --target=resources.c --generate-source
	g++ main.cpp CamadaApresentacao.cpp resources.c -o CalculadoraDojo.out `pkg-config gtkmm-3.0 --cflags --libs` -Wall -pedantic -O0 -ggdb
