#include "CamadaApresentacao.hpp"
#include <iostream>
#include <fstream>
#include <string>

#define USTR_WHERE ( ( (Glib::ustring(__FILE__) + " | ") + __func__) + ":") + std::to_string(__LINE__)
namespace CalculadoraDojo
{
	enum Operacoes{
		Soma=0,

		fim
	};

	CamadaApresentacao::CamadaApresentacao(Glib::RefPtr<Gtk::Application> app) noexcept: app(app)
	{
		try
		{
			Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_resource("/org/anders1232/GtkCodingDojo/JanelaPrincipal.glade");
			window = Glib::RefPtr<Gtk::Window>::cast_dynamic( builder->get_object("MAIN_WINDOW") );
			window->property_title().set_value("Calculadora Dojo Day");

			sbOperador1= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_OPERADOR_1") );
			sbOperador1->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_ALWAYS);
			sbOperador1->set_value(1);
			sbOperador1->set_editable(true);

			sbOperador2= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic( builder->get_object("SPIN_BUTTON_OPERADOR_2") );
			sbOperador2->set_update_policy(Gtk::SpinButtonUpdatePolicy::UPDATE_ALWAYS);
			sbOperador2->set_value(1);
			sbOperador2->set_editable(true);

			cbtOperacao= Glib::RefPtr<Gtk::ComboBoxText>::cast_dynamic( builder->get_object("COMBO_BOX_TEXT_OPERACAO") );
			cbtOperacao->insert(Operacoes::Soma, "+");
			cbtOperacao->set_active(Operacoes::Soma);

			eResultado= Glib::RefPtr<Gtk::Entry>::cast_dynamic( builder->get_object("ENTRY_RESULTADO") );
			eResultado->set_editable(false);

			sbOperador1->signal_value_changed().connect(sigc::mem_fun(*this, &CamadaApresentacao::AtualizarResultados) );
			sbOperador2->signal_value_changed().connect(sigc::mem_fun(*this, &CamadaApresentacao::AtualizarResultados) );
			cbtOperacao->signal_changed().connect(sigc::mem_fun(*this, &CamadaApresentacao::AtualizarResultados) );

			Glib::RefPtr<Gtk::ImageMenuItem> Sair= Glib::RefPtr<Gtk::ImageMenuItem>::cast_dynamic(builder->get_object("BOTAO_SAIR") );
			Sair->signal_activate().connect(sigc::mem_fun(*this, &CamadaApresentacao::Fechar) );

			AtualizarResultados();
			window->present();
		}
		catch(Gtk::BuilderError& error)
		{
			Gtk::MessageDialog( USTR_WHERE + error.what() ,false, Gtk::MessageType::MESSAGE_ERROR );
			exit(EXIT_FAILURE);
		}
		catch(Glib::MarkupError& error)
		{
			Gtk::MessageDialog( USTR_WHERE + error.what(),false, Gtk::MessageType::MESSAGE_ERROR );
			exit(EXIT_FAILURE);
		}
		catch(Gio::ResourceError& error)
		{
			Gtk::MessageDialog( USTR_WHERE+error.what(),false, Gtk::MessageType::MESSAGE_ERROR );
			exit(EXIT_FAILURE);
		}
		std::cout << "Numero de autores: " << ObterAutores().size()<< std::endl;
	}

	CamadaApresentacao::~CamadaApresentacao()
	{}

	void CamadaApresentacao::AtualizarResultados(void)
	{
		double operador1= sbOperador1->get_value();
		double operador2= sbOperador2->get_value();
		int operacao= cbtOperacao->get_active_row_number();

		if(operacao == Operacoes::Soma)
		{
			eResultado->set_text(Glib::ustring(std::to_string(operador1+operador2) ) );
		}
	}

	void CamadaApresentacao::Fechar(void)
	{
		app->quit();
	}

	std::vector< Glib::ustring> CamadaApresentacao::ObterAutores(void) const
	{
		std::vector< Glib::ustring> autores;
		std::ifstream arq;
		arq.open("CONTRIBUTORS", std::ios::in);
		if(arq.is_open())
		{
			std::string linha;
			do
			{
				std::getline(arq, linha);
				if(linha.size() >= 3)
				{
					autores.push_back(linha);
				}
			}
			while(!arq.eof());
		}
		return autores;
	}

	Glib::RefPtr<Gtk::Window> CamadaApresentacao::JanelaPrincipal(void)
	{
		return window;
	}
}
